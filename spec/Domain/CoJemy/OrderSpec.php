<?php

namespace spec\Domain\CoJemy;

use Domain\CoJemy\Aggregate\AggregateId;
use Domain\CoJemy\Order\Events\OrderClosedEvent;
use Domain\CoJemy\Order\Events\OrderOpenedEvent;
use Domain\CoJemy\Order\Prices;
use Domain\CoJemy\Order\Status;
use Domain\CoJemy\Order\HashHolder;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class OrderSpec extends ObjectBehavior
{
    function it_opens_an_order()
    {
        $this->beConstructedWith(AggregateId::fromString('id123'));

        $hashHolder = HashHolder::createFromHashes('adminHash123', 'participantHash123');
        $this->open('shama', $hashHolder, 100.00, 200.00);

        $events = $this->getLatestEvents();
        $events->shouldHaveCount(1);
        $events[0]->shouldBeAnInstanceOf(OrderOpenedEvent::class);
        
        $expectedPrices = new Prices();
        $expectedPrices = $expectedPrices->addDeliveryCost(200.00);
        $expectedPrices = $expectedPrices->addPricePerPackage(100.00);

        $this->getAggregateId()->shouldBeLike(AggregateId::fromString('id123'));
        $this->getSupplierId()->shouldBe('shama');
        $this->getStatus()->shouldBeLike(Status::opened());
        $this->getPrices()->shouldBeLike($expectedPrices);
        $this->getHashHolder()->getAdminHash()->shouldBe('adminHash123');
        $this->getHashHolder()->getParticipantHash()->shouldBe('participantHash123');
    }

    function it_should_recreate_order_from_events()
    {
        $hashHolder = HashHolder::createFromHashes('adminHash123', 'participantHash123');
        $this->beConstructedThrough('recreate', ['id123', [
            new OrderOpenedEvent('id123', 'shama', $hashHolder->toArray(), 100.00, 200.00)
        ]]);

        $expectedPrices = new Prices();
        $expectedPrices = $expectedPrices->addDeliveryCost(200.00);
        $expectedPrices = $expectedPrices->addPricePerPackage(100.00);

        $this->getAggregateId()->shouldBeLike(AggregateId::fromString('id123'));
        $this->getSupplierId()->shouldBe('shama');
        $this->getStatus()->shouldBeLike(Status::opened());
        $this->getPrices()->shouldBeLike($expectedPrices);
        $this->getHashHolder()->getAdminHash()->shouldBe('adminHash123');
        $this->getHashHolder()->getParticipantHash()->shouldBe('participantHash123');
    }

    function it_closes_an_order()
    {
        $hashHolder = HashHolder::createFromHashes('adminHash123', 'participantHash123');
        $this->beConstructedThrough('recreate', ['id123', [
            new OrderOpenedEvent('id123', 'shama', $hashHolder->toArray(), 100.00, 200.00)
        ]]);
        
        $this->close();

        $events = $this->getLatestEvents();
        $events->shouldHaveCount(1);
        $events[0]->shouldBeAnInstanceOf(OrderClosedEvent::class);
        
        $this->getAggregateId()->shouldBeLike(AggregateId::fromString('id123'));
        $this->getSupplierId()->shouldBe('shama');
        $this->getStatus()->shouldBeLike(Status::closed());
    }
}
