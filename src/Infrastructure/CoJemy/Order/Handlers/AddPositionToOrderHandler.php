<?php

namespace Infrastructure\CoJemy\Order\Handlers;

use Domain\CoJemy\Aggregate\AggregateId;
use Domain\CoJemy\EventStorePersister;
use Domain\CoJemy\EventStoreRepository;
use Domain\CoJemy\Order;
use Domain\CoJemy\Order\UserId;
use Infrastructure\CoJemy\Order\Commands\AddPositionToOrderCommand;

class AddPositionToOrderHandler
{
    private $eventStoreRepository;
    private $eventStorePersister;

    /**
     * AddPositionToOrderHandler constructor.
     * @param EventStoreRepository $eventStoreRepository
     * @param EventStorePersister $eventStorePersister
     */
    public function __construct(EventStoreRepository $eventStoreRepository, EventStorePersister $eventStorePersister)
    {
        $this->eventStoreRepository = $eventStoreRepository;
        $this->eventStorePersister = $eventStorePersister;
    }

    /**
     * @param AddPositionToOrderCommand $command
     */
    public function handleAddPositionToOrderCommand(AddPositionToOrderCommand $command)
    {
        $order = $this->eventStoreRepository->findOrderById(AggregateId::fromString($command->getOrderId()));
        $userId = $command->getUserId() ? UserId::fromString($command->getUserId()) : UserId::generate();

        $order->addPosition(
            $command->getDishId(),
            $command->getDishName(),
            $command->getPrice(),
            $userId->__toString(),
            $command->getUserNick()
        );

        $this->eventStorePersister->persist($order->getLatestEvents());
    }
}
