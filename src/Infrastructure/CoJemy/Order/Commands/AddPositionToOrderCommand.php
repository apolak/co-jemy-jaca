<?php

namespace Infrastructure\CoJemy\Order\Commands;

class AddPositionToOrderCommand
{
    private $orderId;
    private $userId;
    private $dishId;
    private $dishName;
    private $price;
    private $userNick;

    public function __construct($orderId, $userId, $dishId, $dishName, $price, $userNick)
    {
        $this->orderId = $orderId;
        $this->userId = $userId;
        $this->dishId = $dishId;
        $this->dishName = $dishName;
        $this->price = $price;
        $this->userNick = $userNick;
    }

    /**
     * @return string
     */
    public function getOrderId() : string
    {
        return $this->orderId;
    }

    /**
     * @return string|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getDishId() : string
    {
        return $this->dishId;
    }

    /**
     * @return string
     */
    public function getDishName() : string
    {
        return $this->dishName;
    }

    /**
     * @return float
     */
    public function getPrice() : float
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getUserNick() : string
    {
        return $this->userNick;
    }
}
