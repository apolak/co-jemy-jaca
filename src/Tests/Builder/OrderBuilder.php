<?php

namespace Tests\Builder;

use Domain\CoJemy\Aggregate\AggregateId;
use Domain\CoJemy\Order;
use Domain\CoJemy\Order\OrderHashGenerator;

class OrderBuilder
{
    const DEFAULT_SUPPLIER_ID = 'shama';
    const DEFAULT_AGGREGATE_ID = 'id123';
    const DEFAULT_PRICE_PACKAGE = '10.00';
    const DEFAULT_DELIVERY_COST = '10.00';

    private $hashHolder;
    private $aggregateId;
    private $supplierId;
    private $pricePackage;
    private $deliveryCost;

    public function __construct()
    {
        $hashGenerator = new OrderHashGenerator();

        $this->hashHolder = $hashGenerator->generate();
        $this->supplierId = self::DEFAULT_SUPPLIER_ID;
        $this->aggregateId = self::DEFAULT_AGGREGATE_ID;
        $this->pricePackage = self::DEFAULT_PRICE_PACKAGE;
        $this->deliveryCost = self::DEFAULT_DELIVERY_COST;
    }

    /**
     * @param string $hash
     * @return OrderBuilder
     */
    public function withHash($hash) : OrderBuilder
    {
        $this->hashHolder = $hash;

        return $this;
    }

    /**
     * @param string $aggregateId
     * @return OrderBuilder
     */
    public function withAggregateId($aggregateId) : OrderBuilder
    {
        $this->aggregateId = $aggregateId;

        return $this;
    }

    /**
     * @param string $supplierId
     * @return OrderBuilder
     */
    public function withSupplier($supplierId) : OrderBuilder
    {
        $this->supplierId = $supplierId;

        return $this;
    }

    /**
     * @param float $pricePackage
     * @return OrderBuilder
     */
    public function withPricePackage($pricePackage) : OrderBuilder
    {
        $this->pricePackage = $pricePackage;

        return $this;
    }

    /**
     * @param float $deliveryCost
     * @return OrderBuilder
     */
    public function withDeliveryCost($deliveryCost) : OrderBuilder
    {
        $this->deliveryCost = $deliveryCost;

        return $this;
    }

    /**
     * @return Order
     */
    public function build() : Order
    {
        $aggregateId = AggregateId::fromString($this->aggregateId);
        $order = new Order($aggregateId);
        $order->open($this->supplierId, $this->hashHolder, $this->pricePackage, $this->deliveryCost);

        return $order;
    }
}
