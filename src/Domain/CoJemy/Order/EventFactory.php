<?php

namespace Domain\CoJemy\Order;

use Domain\CoJemy\Event;

class EventFactory
{
    const EVENT_PATH = "Domain\\CoJemy\\Order\\Events\\";

    /**
     * @param string $eventType
     * @param array $eventParameters
     *
     * @return Event
     */
    public function create($eventType, array $eventParameters) : Event
    {
        $eventClass = self::EVENT_PATH . $eventType;

        if (!class_exists($eventClass)) {
            throw new \RuntimeException(sprintf("Event class %s does not exist.", $eventClass));
        }

        return $eventClass::fromParameters($eventParameters);
    }
}
