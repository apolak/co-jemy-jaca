<?php
/**
 * Created by PhpStorm.
 * User: mszoltysek
 * Date: 28.06.2016
 * Time: 14:33
 */

namespace Domain\CoJemy\Order\Positions;


class PositionId
{
    /**
     * @var string
     */
    private $id;

    /**
     * @param string $id
     */
    private function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return PositionId
     */
    public static function generate() : PositionId
    {
        return new self(uniqid());
    }

    /**
     * @param string $id
     * @return PositionId
     */
    public static function fromString($id) : PositionId
    {
        return new self($id);
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return $this->id;
    }
}