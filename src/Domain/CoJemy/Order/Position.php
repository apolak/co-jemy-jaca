<?php
/**
 * Created by PhpStorm.
 * User: mszoltysek
 * Date: 28.06.2016
 * Time: 14:32
 */

namespace Domain\CoJemy\Order;


use Domain\CoJemy\Order\Positions\PositionId;
use Domain\SharedKernel\Date;

class Position
{
    /** @var  UserId */
    private $userId;

    /** @var  Dish */
    private $dish;

    /** @var  string */
    private $userNick;

    /** @var  PositionId */
    private $id;

    /** @var  Date */
    private $creationDate;

    /**
     * Position constructor.
     * @param $userId
     * @param $dishId
     * @param $dishName
     * @param $price
     * @param string $userNick
     * @param $id
     * @param $creationDate
     */
    public function __construct($userId, $dishId, $dishName, $price, $userNick, $id = null, $creationDate = null)
    {
        $this->userId = UserId::fromString($userId);
        $this->dish = $dishId ? Dish::withId($dishName, $dishId, $price) : Dish::withoutId($dishName, $price);
        $this->userNick = $userNick;
        $this->id = $id ? PositionId::fromString($id) : PositionId::generate();
        $this->creationDate = new Date($creationDate ? $creationDate : 'now');
    }

    /**
     * @return UserId
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return Dish
     */
    public function getDish()
    {
        return $this->dish;
    }

    /**
     * @return string
     */
    public function getUserNick()
    {
        return $this->userNick;
    }

    /**
     * @return PositionId
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Date
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
}