<?php

namespace Domain\CoJemy\Order\Prices;

class Type
{
    const PRICE_PER_PACKAGE = 'pricePerPackage';
    const DELIVERY_COST = 'deliveryCost';
    const DISH_COST = 'dishCost';

    /**
     * @var string
     */
    private $type;

    /**
     * @param string $typeName
     */
    private function __construct($typeName)
    {
        $this->type = $typeName;
    }

    /**
     * @return Type
     */
    public static function pricePerPackage() : Type
    {
        return new self(self::PRICE_PER_PACKAGE);
    }

    /**
     * @return Type
     */
    public static function deliveryCost() : Type
    {
        return new self(self::DELIVERY_COST);
    }

    /**
     * @return Type
     */
    public static function dishCost() : Type
    {
        return new self(self::DISH_COST);
    }

    /**
     * @param Type $other
     * @return bool
     */
    public function isEqualTo(Type $other) : bool
    {
        return $this->type === $other->type;
    }
}
