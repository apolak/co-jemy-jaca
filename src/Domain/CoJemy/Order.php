<?php

namespace Domain\CoJemy;

use Domain\CoJemy\Aggregate\AggregateId;
use Domain\CoJemy\Order\Events\OrderClosedEvent;
use Domain\CoJemy\Order\Events\OrderOpenedEvent;
use Domain\CoJemy\Order\Events\PositionAddedToOrderEvent;
use Domain\CoJemy\Order\Position;
use Domain\CoJemy\Order\Prices;
use Domain\CoJemy\Order\Status;
use Domain\CoJemy\Order\HashHolder;

class Order extends Aggregate
{
    private $supplierId;

    /**
     * @var Status
     */
    private $status;
    private $hashHolder;

    /**
     * @var Prices
     */
    private $prices;

    /** @var Position[] */
    private $positions;

    public function __construct(AggregateId $aggregateId)
    {
        parent::__construct($aggregateId);
        $this->prices = new Prices();
        $this->positions = [];
    }

    /**
     * @param string $aggregateId
     * @param Event[] $events
     * @return Order
     */
    public static function recreate($aggregateId, array $events) : Order
    {
        $order = new self(AggregateId::fromString($aggregateId));

        foreach($events as $event) {
            $order->apply($event);
        }

        return $order;
    }

    /**
     * @param $supplierId
     * @param HashHolder $hashHolder
     * @param $pricePerPackage
     * @param $deliverCost
     */
    public function open($supplierId, HashHolder $hashHolder, $pricePerPackage, $deliverCost)
    {
        $event = new OrderOpenedEvent((string) $this->id, $supplierId, $hashHolder->toArray(), $pricePerPackage, $deliverCost);
        $this->latestEvents[] = $event;
        $this->apply($event);
    }

    /**
     * @param OrderOpenedEvent $event
     */
    protected function applyOrderOpenedEvent(OrderOpenedEvent $event)
    {
        $bag = $event->getParametersBag();

        $this->supplierId = $bag->getParameter('supplierId');
        $this->prices = $this->prices->addPricePerPackage($bag->getParameter('pricePerPackage'));
        $this->prices = $this->prices->addDeliveryCost($bag->getParameter('deliveryCost'));
        
        $hashes = $bag->getParameter('hashes');
        
        $this->hashHolder = HashHolder::createFromHashes($hashes['adminHash'], $hashes['participantHash']);
        
        $this->status = Status::opened();
    }

    protected function applyOrderClosedEvent(OrderClosedEvent $event)
    {
        $this->status = Status::closed();
    }
    
    public function getSupplierId() : string
    {
        return $this->supplierId; 
    }

    /**
     * @return Status
     */
    public function getStatus() : Status
    {
        return $this->status;
    }

    /**
     * @return Prices
     */
    public function getPrices() : Prices
    {
        return $this->prices;
    }

    public function close()
    {
        $event = new OrderClosedEvent((string) $this->id);
        $this->latestEvents[] = $event;
        $this->apply($event);
    }
    
    public function getHashHolder() : HashHolder
    {
        return $this->hashHolder;
    }

    public function addPosition(string $dishId, string $dishName, float $price, string $userId, string $userNick)
    {
        $event = new PositionAddedToOrderEvent((string) $this->id, $userId, $dishId, $dishName, $price, $userNick);
        $this->latestEvents[] = $event;
        $this->apply($event);
    }

    /**
     * @return array|Order\Position[]
     */
    public function getPositions() : array
    {
        return $this->positions;
    }

    protected function applyPositionAddedToOrderEvent(PositionAddedToOrderEvent $event)
    {
        $bag = $event->getParametersBag();
        $position = new Position(
            $bag->getParameter('userId'),
            $bag->getParameter('dishId'),
            $bag->getParameter('dishName'),
            $bag->getParameter('price'),
            $bag->getParameter('userNick')
        );

        $this->positions[] = $position;
    }
}
