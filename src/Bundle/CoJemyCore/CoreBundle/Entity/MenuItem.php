<?php

namespace Bundle\CoJemyCore\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class MenuItem
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $price;

    /**
     * @var FoodSupplier
     */
    private $foodSupplier;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     *
     * @return MenuItem
     */
    public function setName($name) : MenuItem
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $price
     *
     * @return MenuItem
     */
    public function setPrice($price) : MenuItem
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param FoodSupplier $foodSupplier
     *
     * @return MenuItem
     */
    public function setFoodSupplier(FoodSupplier $foodSupplier = null) : MenuItem
    {
        $this->foodSupplier = $foodSupplier;

        return $this;
    }

    /**
     * @return FoodSupplier
     */
    public function getFoodSupplier()
    {
        return $this->foodSupplier;
    }
}
