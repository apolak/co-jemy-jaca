<?php

namespace Features\Domain;

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Symfony2Extension\Context\KernelDictionary;

use Infrastructure\CoJemy\Order\Commands\AddPositionToOrderCommand;
use Infrastructure\CoJemy\Order\Handlers\AddPositionToOrderHandler;
use League\Tactician\Handler\CommandNameExtractor\ClassNameExtractor;
use League\Tactician\Handler\Locator\InMemoryLocator;
use League\Tactician\Handler\MethodNameInflector\HandleClassNameInflector;
use League\Tactician\Handler\CommandHandlerMiddleware;
use League\Tactician\CommandBus;

use Domain\CoJemy\Aggregate\AggregateId;
use Domain\CoJemy\Order\OrderHashGenerator;
use Domain\CoJemy\Order;

use Infrastructure\CoJemy\Order\Handlers\OpenOrderHandler;
use Infrastructure\CoJemy\Order\Handlers\CloseOrderHandler;
use Infrastructure\CoJemy\Order\Commands\OpenOrderCommand;
use Infrastructure\CoJemy\Order\Commands\CloseOrderCommand;

use Tests\Builder\OrderBuilder;
use Tests\Repository\DatabaseEventStore;

/**
 * Defines application features from the specific context.
 */
class DeveloperContext implements Context, SnippetAcceptingContext
{
    use KernelDictionary;

    private $eventStore;
    private $hashGenerator;

    /** @var  CommandBus */
    private $commandBus;

    /** @BeforeScenario */
    public function beforeScenario()
    {
        $this->eventStore = new DatabaseEventStore($this->getContainer()->get('doctrine.orm.entity_manager'));
        $this->hashGenerator = new OrderHashGenerator();
        $locator = new InMemoryLocator();
        $locator->addHandler(new OpenOrderHandler($this->eventStore, $this->hashGenerator), OpenOrderCommand::class);
        $locator->addHandler(new CloseOrderHandler($this->eventStore, $this->eventStore), CloseOrderCommand::class);
        $locator->addHandler(new AddPositionToOrderHandler($this->eventStore, $this->eventStore), AddPositionToOrderCommand::class);
        $handlerMiddleware = new CommandHandlerMiddleware(
            new ClassNameExtractor(),
            $locator,
            new HandleClassNameInflector()
        );
        $this->commandBus = new CommandBus([$handlerMiddleware]);
    }

    /**
     * @Given there are no orders in the system
     */
    public function thereAreNoOrdersInTheSystem()
    {
        $this->eventStore->clear();
    }

    /**
     * @When I open new order for supplier :supplierId with price per package :pricePerPackage and delivery cost :deliveryCost
     */
    public function iOpenNewOrderForSupplier($supplierId, $pricePerPackage, $deliveryCost)
    {
        $this->commandBus->handle(new OpenOrderCommand($supplierId, $pricePerPackage, $deliveryCost));
    }

    /**
     * @Then there should be :ordersNumber order with status :status
     */
    public function thereShouldBeOrderWithStatus($ordersNumber, $status)
    {
        if ($this->eventStore->count() != $ordersNumber) {
            throw new \RuntimeException('Bad count of orders');
        }

        if ($this->eventStore->getOrder()->getStatus() != $status) {
            throw new \RuntimeException('Bad status');
        }
    }

    /**
     * @Then order should be placed for supplier :supplierName
     */
    public function orderShouldBePlacedForSupplier($supplierName)
    {
        if ($this->eventStore->getOrder()->getSupplierId() != $supplierName) {
            throw new \RuntimeException('Bad supplier');
        }
    }

    /**
     * @Given there is order in the system with id :aggregateId
     */
    public function thereIsOrderInTheSystemWithId($aggregateId)
    {
        $order = (new OrderBuilder())
            ->withAggregateId($aggregateId)
            ->build();

        $this->eventStore->persist($order->getLatestEvents());
    }

    /**
     * @When I close order with id :aggregateId
     */
    public function iCloseOrderWithId($aggregateId)
    {
        $this->commandBus->handle(new CloseOrderCommand(AggregateId::fromString($aggregateId)));
    }

    /**
     * @Given /^order admin hash should not be null$/
     */
    public function orderAdminHashShouldNotBeNull()
    {
        if (is_null($this->eventStore->getOrder()->getHashHolder()->getAdminHash())) {
            throw new \RuntimeException('Admin hash is null');
        }
    }

    /**
     * @Given /^order participant hash should not be null$/
     */
    public function orderParticipantHashShouldNotBeNull()
    {
        if (is_null($this->eventStore->getOrder()->getHashHolder()->getParticipantHash())) {
            throw new \RuntimeException('Participant hash is null');
        }
    }

    /**
     * @Then order should have delivery cost set to :deliveryCost and price per package to :pricePerPackage
     */
    public function orderShouldHaveDeliveryCostSetToAndPricePerPackageTo($deliveryCost, $pricePerPackage)
    {
        if ($deliveryCost != $this->eventStore->getOrder()->getPrices()->getDeliveryCost()->getAmount()) {
            throw new \RuntimeException('Delivery cost is not equal to ' . $deliveryCost);
        }

        if ($pricePerPackage != $this->eventStore->getOrder()->getPrices()->getPricePerPackage()->getAmount()) {
            throw new \RuntimeException('Price per package is not equal to ' . $pricePerPackage);
        }
    }

    /**
     * @When user :userId adds position with :dishName, :dishId and :dishPrice to order :orderId for :userNick
     */
    public function userAddsPositionWithAndToOrderFor($userId, $dishName, $dishId, $dishPrice, $orderId, $userNick)
    {
        $this->commandBus->handle(new AddPositionToOrderCommand($orderId, $userId, $dishId, $dishName, $dishPrice, $userNick));
    }

    /**
     * @Then there should be :ordersNumber order with :count position
     */
    public function thereShouldBeOrderWithPosition($ordersNumber, $count)
    {
        if ($this->eventStore->count() != $ordersNumber) {
            throw new \RuntimeException('Bad count of orders');
        }

        if (count($this->eventStore->getOrder()->getPositions()) != $count) {
            throw new \RuntimeException('Bad count of positions');
        }
    }

    /**
     * @Then the position should be :dishName, :dishId, :dishPrice owned by :userNick
     */
    public function thePositionShouldBeOwnedBy($dishName, $dishId, $dishPrice, $userNick)
    {
        /** @var Order $order */
        $order = $this->eventStore->getOrder();
        $position = $order->getPositions()[0];

        if ($position->getDish()->getName() != $dishName) {
            throw new \RuntimeException('Bad dish name');
        }

        if ($position->getDish()->getId() != $dishId) {
            throw new \RuntimeException('Bad dish id');
        }

        if ($position->getDish()->getPrice()->getAmount() != $dishPrice) {
            throw new \RuntimeException('Bad dish price');
        }

        if ($position->getUserNick() != $userNick) {
            throw new \RuntimeException('Bad user nick for dish');
        }
    }

    /**
     * @When noid user adds position with :dishName, :dishId and :dishPrice to order :orderId for :userNick
     */
    public function iNoIdUserAddPositionWithAndToOrderFor($dishName, $dishId, $dishPrice, $orderId, $userNick)
    {
        $this->commandBus->handle(new AddPositionToOrderCommand($orderId, null, $dishId, $dishName, $dishPrice, $userNick));
    }

    /**
     * @Then the userId should be string
     */
    public function theUserIdShouldBeString()
    {
        /** @var Order $order */
        $order = $this->eventStore->getOrder();
        $position = $order->getPositions()[0];

        if (!$position->getUserId() instanceof Order\UserId) {
            throw new \RuntimeException('User id is not instance of the class UserId');
        }

        if (!is_string($position->getUserId()->__toString())) {
            throw new \RuntimeException('User id is not string');
        }

        if (strlen($position->getUserId()->__toString()) == 0) {
            throw new \RuntimeException('User id is an empty string');
        }
    }

}
